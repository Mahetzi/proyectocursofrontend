import React, { Component,Fragment } from 'react'

export default class Home extends Component {
    constructor(){
        super();
        console.log("constructor");
        this.state = {
            email: "ejemplo@algo.com",
            password: "",
            session: false
        }
    }
    componentDidMount = () => { 
        console.log("componetnDidMount");
    }
    componentDidUpdate =(prevState, prevProps) =>{
        //console.log("componentDidUpdate", prevProps, prevState)
    }
    componentWillUnmount=()=>{
        console.log("componentWillUnmount")
    }
    handleClickButon = () =>{
      if(this.state.password && this.state.email){
        !/\S+@\S+\.\S+/.test(this.state.email) ? alert("correo invalido") : 
      this.setState({
          session: !this.state.session
      })
    }else{
       this.state.email ? 
       alert("ingrese el password")
       :
       alert("ingrese el email ")
      }
    }

    handleChangeValues=(e)=>{
        console.log(e.target.name, e.target.value)
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    render() {
        return (
            <Fragment>
                {
                    !this.state.session &&
                <form autoComplete="off">
                   <div>
                    <label>
                        ingresa tu correo
                    </label>
                    <input name="email" onChange={this.handleChangeValues} value={this.state.email} type="text" />
                    </div> 
                    <div>
                        <label>
                           ingresa tu password 
                        </label>
                        <input name="password" onChange={this.handleChangeValues} value={this.state.password} type="password"/>
                    </div>
                    <button type="reset" onClick={this.handleClickButon}>
                        aceptar
                    </button>
                </form>
                }
                <div>
                    {
                        this.state.session &&
                       <div>
                           <h1> 
                               Bienvenido usuario {this.state.email} 
                           </h1>
                           <button onClick={this.handleClickButon}>
                               cerrar session
                           </button>
                        </div>   
                    }
                </div>
            </Fragment>
        )
    }
}
