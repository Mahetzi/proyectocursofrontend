import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.css</code> and save to reload.
        </p>
        <input></input>
        <label>hola mundo </label>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        > 
         Hola mundo esta es mi primera practica de react js 
        </a>
        <button>
            aceptar
        </button>
      </header>
    </div>
  );
}

export default App;
